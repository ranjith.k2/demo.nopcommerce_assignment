package Asignment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;
import java.util.Iterator;

import org.apache.commons.math3.stat.descriptive.summary.Product;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class Demo_Nopcommerc {
	WebDriver driver = null;

	@BeforeClass
	public void launchApplications() {
		driver = new ChromeDriver();
		driver.get("https://admin-demo.nopcommerce.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

	}

	@Test
	public void Login() {

		driver.findElement(By.xpath("//button[@type='submit']")).click();

		String text = driver.findElement(By.partialLinkText("John Smith")).getText();
		Assert.assertEquals(text, "John Smith");
	}

	@Test(priority = 1)

	public void Categories() throws InterruptedException, BiffException, IOException {

		driver.findElement(By.partialLinkText("Catalog")).click();
		driver.findElement(By.partialLinkText("Categories")).click();
		driver.findElement(By.partialLinkText("Add new")).click();
		File f = new File("/home/ranjith/eclipse-workspace/Demo_nopcommerc/src/test/resources/data/demodata.xlsx");
		FileInputStream files = new FileInputStream(f);
		XSSFWorkbook workbook = new XSSFWorkbook(files);
		XSSFSheet Sheet1 = workbook.getSheetAt(0);
		int row = Sheet1.getPhysicalNumberOfRows();
		for (int i = 0; i < row; i++) {
			String name = Sheet1.getRow(i).getCell(0).getStringCellValue();
			String pass = Sheet1.getRow(i).getCell(1).getStringCellValue();

			driver.findElement(By.id("Name")).sendKeys(name);
			Thread.sleep(3000);
			driver.switchTo().frame(0);

			driver.findElement(By.id("tinymce")).sendKeys(pass);
			driver.switchTo().defaultContent();
			Thread.sleep(3000);

			WebElement element = driver.findElement(By.id("ParentCategoryId"));
			Select s = new Select(element);
			s.selectByIndex(5);
			driver.findElement(By.name("save")).click();
			driver.findElement(By.id("SearchCategoryName")).sendKeys(name);
			driver.findElement(By.id("search-categories")).click();

		}

		String text = driver.findElement(By.xpath("//tr[@class='odd']")).getText();
		Assert.assertEquals(text, "Build your own computer");

	}

	@Test(priority = 2)
	private void Product() throws InterruptedException {
		driver.findElement(By.linkText("Products")).click();
		driver.findElement(By.id("SearchProductName")).sendKeys("Build your own computer");
		WebElement produ = driver.findElement(By.id("SearchCategoryId"));
		Select sel = new Select(produ);
		sel.selectByVisibleText("Computers >> Desktops");

		driver.findElement(By.id("search-products")).click();

		String text = driver.findElement(By.xpath("//tr[@class='odd']")).getText();
		Assert.assertEquals(text, "Build your own computer");

	}

	@Test(priority = 3)
	public void Manufacturers() throws InterruptedException, IOException {

		driver.findElement(By.partialLinkText("Manufacturers")).click();
		driver.findElement(By.partialLinkText("Add new")).click();
		File f = new File("/home/ranjith/eclipse-workspace/Demo_nopcommerc/src/test/resources/data/demodata.xlsx");
		FileInputStream files = new FileInputStream(f);
		XSSFWorkbook workbook = new XSSFWorkbook(files);
		XSSFSheet Sheet1 = workbook.getSheetAt(0);
		int row = Sheet1.getPhysicalNumberOfRows();
		for (int i = 0; i < row; i++) {
			String name = Sheet1.getRow(i).getCell(0).getStringCellValue();
			String pass = Sheet1.getRow(i).getCell(1).getStringCellValue();

			driver.findElement(By.id("Name")).sendKeys(name);
			Thread.sleep(3000);

			driver.switchTo().frame(0);
			driver.findElement(By.id("tinymce")).sendKeys(name);
			driver.switchTo().defaultContent();
			Thread.sleep(3000);

			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,500)");

			driver.findElement(By.id("manufacturer-display")).click();

			driver.findElement(By.xpath("//input[@title=\"0.0000 USD\"]")).sendKeys("50");

			driver.findElement(By.xpath("//input[@title=\"10000.0000 USD\"]")).sendKeys("70");
			driver.findElement(By.xpath("//input[@title=\"0 \"]")).sendKeys("1");
			driver.findElement(By.name("save")).click();
			driver.findElement(By.name("SearchManufacturerName")).sendKeys("Build your own computer");
			driver.findElement(By.id("search-manufacturers")).click();
		}
		String text = driver.findElement(By.xpath("//tr[@class='odd']")).getText();
		Assert.assertEquals(text, "Build your own computer");

	}

	@Test(priority = 4)
	public void Logout() {
		driver.findElement(By.partialLinkText("Logout")).click();

	}

	@AfterClass
	public void closeBrowser() {

		driver.close();
	}
}
